from edgar3 import __version__, edgar_index
import datetime
import time


def test_version():
    assert __version__ == "edgar3 version 1.2.1"


def test_get_quarter():
    time.sleep(1)
    ed = edgar_index.edgar_index()
    assert ed._get_quarter(1) == 1
    assert ed._get_quarter(2) == 1
    assert ed._get_quarter(3) == 1
    assert ed._get_quarter(4) == 2
    assert ed._get_quarter(5) == 2
    assert ed._get_quarter(6) == 2
    assert ed._get_quarter(7) == 3
    assert ed._get_quarter(8) == 3
    assert ed._get_quarter(9) == 3
    time.sleep(1)
    assert ed._get_quarter(10) == 4
    assert ed._get_quarter(11) == 4
    assert ed._get_quarter(12) == 4
    assert ed._get_quarter(0) == 1
    assert ed._get_quarter(13) == 4
